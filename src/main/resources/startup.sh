#!/bin/bash
#-Dlogback.configurationFile="$home/config/$logger"


JAVA_OPTS="-Xms256m -Xmx1024m"
logger="logback.xml"
declare home="/opt/fundamental/config/logback.xml"

nohup java $JAVA_OPTS -Dspring.config.location=/opt/fundamental/config/application.properties -jar /opt/fundamental/fundamental-0.0.1-SNAPSHOT.jar > /opt/fundamental/log.txt 2>&1 &
echo $! > /opt/fundamental/pid.file
