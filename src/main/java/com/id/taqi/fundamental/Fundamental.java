package com.id.taqi.fundamental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fundamental {
	
	public static void main(String[] args) {
		SpringApplication.run(Fundamental.class, args);
	}
}
