package com.id.taqi.fundamental.common.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "datauntuktable")
public class DataUntukTable {

	@Id
	private int id;
	private String nama;
	private String bagian;
	private String kontak;
	private String alamat;
	private String keterangan;
	private Date tanggal;
	@Override
	public String toString() {
		return "DataUntukTable [id=" + id + ", nama=" + nama + ", bagian=" + bagian + ", kontak=" + kontak + ", alamat="
				+ alamat + ", keterangan=" + keterangan + ", tanggal=" + tanggal + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getBagian() {
		return bagian;
	}
	public void setBagian(String bagian) {
		this.bagian = bagian;
	}
	public String getKontak() {
		return kontak;
	}
	public void setKontak(String kontak) {
		this.kontak = kontak;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public Date getTanggal() {
		return tanggal;
	}
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
}
