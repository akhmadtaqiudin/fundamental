package com.id.taqi.fundamental.common.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.taqi.fundamental.common.model.ParamConfig;

public interface ParamConfigRepository extends JpaRepository<ParamConfig, Integer>{

	ParamConfig findByStatusConfig(String statusConfig);
}
