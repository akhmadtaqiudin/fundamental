package com.id.taqi.fundamental.common.repositorys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.taqi.fundamental.common.model.ParamMapping;

public interface ParamMappingRepository extends JpaRepository<ParamMapping, Integer>{

	List<ParamMapping> findByIdData(int idData);
}
