package com.id.taqi.fundamental.common.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.fundamental.common.repositorys.DataUntukTableRepository;

@RestController
@RequestMapping(value = "/api/test")
public class DataUntukTableController {

	@Autowired
	private DataUntukTableRepository dtr;
	
	@RequestMapping(value="/data", method=RequestMethod.GET)
	public LinkedHashMap<String, Object> getAllData(){
		
		LinkedHashMap<String, Object> rest = new LinkedHashMap<>();
		
		rest.put("Message", "Success");
		rest.put("Response", dtr.findAll());
		
		return rest;
	}
}
