package com.id.taqi.fundamental.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="paramconfig")
public class ParamConfig {

	@Id
	private int id;
	@Column(name="id_data")
	private int idData;
	@Column(name="nama_config")
	private String namaConfig;
	@Column(name="status_config")
	private String statusConfig;
	@Override
	public String toString() {
		return "ParamConfig [id=" + id + ", idData=" + idData + ", namaConfig=" + namaConfig + ", statusConfig="
				+ statusConfig + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdData() {
		return idData;
	}
	public void setIdData(int idData) {
		this.idData = idData;
	}
	public String getNamaConfig() {
		return namaConfig;
	}
	public void setNamaConfig(String namaConfig) {
		this.namaConfig = namaConfig;
	}
	public String getStatusConfig() {
		return statusConfig;
	}
	public void setStatusConfig(String statusConfig) {
		this.statusConfig = statusConfig;
	}
}
