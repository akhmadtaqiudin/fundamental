package com.id.taqi.fundamental.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parammapping")
public class ParamMapping {

	@Id
	private int id;
	@Column(name="id_data")
	private int idData;
	@Column(name="request_response_code")
	private String requestResponseCode;
	@Column(name="request_response_desc")
	private String requestResponseDesc;
	@Column(name="status_response_code")
	private String statusResponseCode;
	@Column(name="status_response_desc")
	private String statusResponseDesc;
	@Column(name="type_mapping")
	private String typeMapping;
	@Override
	public String toString() {
		return "ParamMapping [id=" + id + ", idData=" + idData + ", requestResponseCode=" + requestResponseCode
				+ ", requestResponseDesc=" + requestResponseDesc + ", statusResponseCode=" + statusResponseCode
				+ ", statusResponseDesc=" + statusResponseDesc + ", typeMapping=" + typeMapping + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdData() {
		return idData;
	}
	public void setIdData(int idData) {
		this.idData = idData;
	}
	public String getRequestResponseCode() {
		return requestResponseCode;
	}
	public void setRequestResponseCode(String requestResponseCode) {
		this.requestResponseCode = requestResponseCode;
	}
	public String getRequestResponseDesc() {
		return requestResponseDesc;
	}
	public void setRequestResponseDesc(String requestResponseDesc) {
		this.requestResponseDesc = requestResponseDesc;
	}
	public String getStatusResponseCode() {
		return statusResponseCode;
	}
	public void setStatusResponseCode(String statusResponseCode) {
		this.statusResponseCode = statusResponseCode;
	}
	public String getStatusResponseDesc() {
		return statusResponseDesc;
	}
	public void setStatusResponseDesc(String statusResponseDesc) {
		this.statusResponseDesc = statusResponseDesc;
	}
	public String getTypeMapping() {
		return typeMapping;
	}
	public void setTypeMapping(String typeMapping) {
		this.typeMapping = typeMapping;
	}
}
