package com.id.taqi.fundamental.common.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.id.taqi.fundamental.common.model.DataUntukTable;

public interface DataUntukTableRepository extends JpaRepository<DataUntukTable, Integer>{

}
