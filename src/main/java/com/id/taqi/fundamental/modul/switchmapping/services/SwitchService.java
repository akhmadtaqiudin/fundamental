package com.id.taqi.fundamental.modul.switchmapping.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.id.taqi.fundamental.common.model.DataUntukTable;
import com.id.taqi.fundamental.common.model.ParamConfig;
import com.id.taqi.fundamental.config.CustomConfig;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

@Service
public class SwitchService {

	private static final Logger logger = LoggerFactory.getLogger(SwitchService.class);
	
	@Autowired
	private Environment env;
	
	CustomConfig cc;
	ParamConfig pc;
	private String nm;
	private String ktk;
	private String almt;
	
	public Object hitToCoreSystem(HashMap<String, Object> requestBody) throws JsonGenerationException, JsonMappingException, IOException {
		LinkedHashMap<String, Object> rest;
		ObjectMapper json = new ObjectMapper();
		init(requestBody);
		logger.info("[ Request ]"+requestBody);
		String responseFromCore = hitCoreSystem();
		logger.info("[ Response System ]"+responseFromCore);
		
		rest = mappingResponse(responseFromCore);
		logger.info("[ Response to Request ]"+rest);
		return json.writeValueAsString(rest);
	}

	private void init(HashMap<String, Object> requestBody) {
		cc = new CustomConfig();
		for(DataUntukTable dt : cc.getDt()) {
			if(dt.getNama().equals("nama 9")) {
				nm = dt.getNama();
				almt = dt.getAlamat();
			}
		}
		
		ktk = requestBody.get("kon").toString();
		pc = cc.getParam();
	}

	private String hitCoreSystem() {
		logger.info("[Start hit to Core System]");
		
		String responseCore = null;
		try {
			OkHttpClient okHttpClient = new OkHttpClient();
			okHttpClient.setConnectTimeout(90, TimeUnit.SECONDS);
			okHttpClient.setReadTimeout(90, TimeUnit.SECONDS);
			com.squareup.okhttp.RequestBody body = com.squareup.okhttp.RequestBody
					.create(MediaType.parse("application/json; charset=utf-8"), initMappingRequest());
			logger.info("URL Hit = "+env.getProperty("HITEXAMPLE"));

			Request r = new Request.Builder().url(env.getProperty("HITEXAMPLE")).post(body).build();
			Response response = okHttpClient.newCall(r).execute();
			responseCore = response.body().string();
		} catch (Exception e) {
			logger.error("[hit to Core System = "+e.getMessage()+ ", Trace : " + e.getStackTrace()[0]);
		}
		logger.info("[End hit to Core System]");
		return responseCore;
	}

	private String initMappingRequest() {
		JSONObject json = new JSONObject();
		try {
			json.put("namaPerson", nm);
			json.put("alamatPerson", almt);
			json.put("kontakPerson", ktk);
			logger.info("[Request to System] "+json.toString());
		} catch (Exception e) {
			logger.error("[initHit to Core System = "+e.getMessage()+ ", Trace : " + e.getStackTrace()[0]);
		}
		return json.toString();
	}

	private LinkedHashMap<String, Object> mappingResponse(String responseFromCore) {
		LinkedHashMap<String, Object> rest = new LinkedHashMap<>();
		logger.debug("mapping rest"+responseFromCore);
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseFromCore);
			JSONObject jsonObject = new JSONObject(obj.toString());
			try {
				if(jsonObject.get("Status").toString().equals("Success")) {
					rest.put("Response Code ", pc.getIdData());
					rest.put("Response Desc ", pc.getNamaConfig());
				}else {
					rest.put("Response Code ", jsonObject.get("Status").toString());
					rest.put("Response Desc ", jsonObject.get("Message").toString());
				}
				
			} catch (Exception e) {
				rest.put("Response Code ", env.getProperty("RESPONSE_CODE"));
				rest.put("Response Desc ", env.getProperty("RESPONSE_DESC"));
			}
			
			JSONObject child = null;
			try {
				child = (JSONObject) ((jsonObject.get("Response") == null) ? null : jsonObject.get("Response"));
				rest.put("Id Daftar ", child.get("id").toString());
				rest.put("Nama ", child.get("namaPerson").toString());
				rest.put("Alamat ", child.get("alamatPerson").toString());
				rest.put("Kontak ", child.get("kontakPerson").toString());
			} catch (Exception e) {
				rest.put("Id Daftar ", "");
				rest.put("Nama ", "");
				rest.put("Alamat ", "");
				rest.put("Kontak ", "");
			}
		} catch (ParseException e) {
			logger.error("[initHit to Core System = "+e.getMessage()+ ", Trace : " + e.getStackTrace()[0]);
		}
		return rest;
	}

}
