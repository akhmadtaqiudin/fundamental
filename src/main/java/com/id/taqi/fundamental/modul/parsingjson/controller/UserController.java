package com.id.taqi.fundamental.modul.parsingjson.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.id.taqi.fundamental.config.CustomConfig;
import com.id.taqi.fundamental.modul.parsingjson.model.User;

@RestController
@RequestMapping(value = "/api")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	private CustomConfig cc;
	@RequestMapping(value = "/par", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
	public String getPar(@RequestBody User user) throws JsonProcessingException {
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rest = new HashMap<>();
		//User user = createDummyUser();
		
		try {
			//Write file from request
			mapper.writeValue(new File("D:\\logs\\user.json"), user);
			
			//Convert object to JSON string

			String jsonInString = mapper.writeValueAsString(user);
			logger.info("[Convert Object to JSON String] "+jsonInString);	
			
			System.out.println("\n====== untuk config =======\n");
			cc = new CustomConfig();
			System.out.println("name dari config = "+cc.getParam());
			System.out.println("Message dari config = "+cc.getParam());
			
			System.out.println("\n=========================\n");
			
			// Convert JSON string from file to Object
			user = mapper.readValue(jsonInString, User.class);
			//Read file from path variable
			User u = mapper.readValue(new File("D:\\logs\\user.json"), User.class);
			logger.info("[Convert JSON string to Object] "+user);
			logger.info("[Read File Convert JSON String to Object] "+u);
			logger.info("[Read File Convert Object to JSON String] "+mapper.writeValueAsString(u));
			
			rest = mapper.readValue(jsonInString, new TypeReference<Map<String, Object>>(){});
			logger.debug("Map = nama : "+rest.get("name")+", age : "+rest.get("age"));
			
		} catch (Exception e) {
			logger.error("[Error Mapper] ===>" +e.getMessage() + ", Trace : " + e.getStackTrace()[0]);
		}
		return mapper.writeValueAsString(user);
	}
}
