package com.id.taqi.fundamental.modul.switchmapping.controller;

import java.io.IOException;
import java.util.HashMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.fundamental.modul.switchmapping.services.SwitchService;

@RestController
@RequestMapping(value="/api")
public class SwitchController {

	@Autowired
	private SwitchService service;
	
	@RequestMapping(value="/switch",method=RequestMethod.POST,produces=MediaType.TEXT_HTML_VALUE)
	public Object hitToCore(@RequestBody HashMap<String, Object> requestBody) throws JsonGenerationException, JsonMappingException, IOException{
		return service.hitToCoreSystem(requestBody);
	}
}
