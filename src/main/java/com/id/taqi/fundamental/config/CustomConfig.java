package com.id.taqi.fundamental.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.id.taqi.fundamental.Fundamental;
import com.id.taqi.fundamental.common.model.DataUntukTable;
import com.id.taqi.fundamental.common.model.ParamConfig;
import com.id.taqi.fundamental.common.model.ParamMapping;
import com.id.taqi.fundamental.common.repositorys.DataUntukTableRepository;
import com.id.taqi.fundamental.common.repositorys.ParamConfigRepository;
import com.id.taqi.fundamental.common.repositorys.ParamMappingRepository;

@Component
public class CustomConfig implements CommandLineRunner{

	private static final Logger logger = LoggerFactory.getLogger(Fundamental.class);
	
	@Autowired
	private DataUntukTableRepository dtr;
	@Autowired
	private  ParamConfigRepository pcr;
	@Autowired
	private  ParamMappingRepository pmr;
	private static ParamConfig param;
	private static List<DataUntukTable> dt = new ArrayList<>();
	private static List<ParamMapping> pm = new ArrayList<>();
	@Override
	public void run(String... args) throws Exception {
		dt = dtr.findAll();
		pm = pmr.findByIdData(17);
		param = pcr.findByStatusConfig("active");
		logger.info("[Data Config Param]"+param);
		logger.info("[Data Config Data]"+dt);
		logger.info("[Data Config Mapping]"+pm);
	}
	public ParamConfig getParam() {
		return param;
	}
	public List<DataUntukTable> getDt() {
		return dt;
	}
	public List<ParamMapping> getPm() {
		return pm;
	}
}
